﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odbior2
{
    public partial class Form1 : Form
    {
        int x, y;
        String wymiar;
        public Form1()
        {
            Thread t1 = new Thread(Pobranie);

            InitializeComponent();
            t1.Start();
        }

        public void Pobranie()
        { 
            IPHostEntry hostDnsEntry = Dns.GetHostEntry("localhost");
            IPAddress serverIp = hostDnsEntry.AddressList[0];

            l2.Invoke(new Action(delegate ()
            {
                l2.Text = "odpalono";
            }));

            Socket daytimeSocket = new Socket(
                serverIp.AddressFamily,
                SocketType.Stream,
                ProtocolType.Tcp);
            daytimeSocket.Connect(serverIp, 49153);
            
            using (Stream daneUsluga = new NetworkStream(daytimeSocket, true))
            using (StreamReader daneCzytnik = new StreamReader(daneUsluga))
            {
                wymiar = daneCzytnik.ReadToEnd();
             }

            l1.Invoke(new Action(delegate ()
            {
                l1.Text = wymiar;
            }));

        }
    }
}

