﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Okienko2
{
    public partial class Form1 : Form
    {
        delegate void ustawRozdzielczosc();
        delegate void wpiszTekstdoStrumienia();
        delegate void wypiszTekst();
        delegate void SetTextCallback(string text);
        delegate void ustawMysz(int xM, int yM);
        delegate void wypiszWylonczanie();

        private int xK, yK;
        private Socket daytatimeListener;
        private NetworkStream connectionStream;
        private int xM = 0, yM = 0;
        private Thread t1, t2;

        private string ciag = " ", tekstDoWypisania;


        //This is a replacement for Cursor.Position in WinForms
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern bool SetCursorPos(int x, int y);

        [DllImport("user32.dll")]
        public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);
        //1. flagi przycisków
        //2 i 3 -gdzie kliknięto
        //4 - kombinacja kliknęć - ekstra metadata
        //musimy wysłać down i up
        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;
        private const int MOUSEEVENTF_MIDDLEDOWN = 0x20;
        private const int MOUSEEVENTF_MIDDLEUP = 0x40;



        public Form1()
        {
            InitializeComponent();

            t1 = new Thread(Pobierz);
            t2 = new Thread(Polaczenie);

            t1.Start();
            t2.Start();

            this.l6.Text = GetLocalIPAddress();
        }

        //pobieranie adresu IP
        public string GetLocalIPAddress()
        {
             this.l5.Text = "Adres IP Twójego komupeta: ";
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        return ip.ToString();
                    }
                 }
            throw new Exception("Nie ma połączeń z przydzielonym adresem IPv4");
        }

            

        private void Pobierz()
        {
            xK = Screen.PrimaryScreen.Bounds.Width;
            yK = Screen.PrimaryScreen.Bounds.Height;

            if (this.l2.InvokeRequired)
            {
                ustawRozdzielczosc d = new ustawRozdzielczosc(Pobierz);
                this.Invoke(d, new object[] { });
            }
            else
            {
                this.l2.Text = xK.ToString() + " x " + yK.ToString();
            }

            if (this.l1.InvokeRequired)
            {
                wypiszTekst a = new wypiszTekst(Pobierz);
                this.Invoke(a, new object[] { });
            }
            else
            {
                this.l1.Text = "Rodzielczość ekranu";
            }

        }


        //poruszanie kursorem myszy musiałem zrobić w delegacie, ponieważ to obcy wątek

        private void MoveCursor(int xM, int yM)
        {
            if (this.l4.InvokeRequired)
            {
                ustawMysz m = new ustawMysz(MoveCursor);
                this.Invoke(m, new object[] { xM, yM });
            }
            else
            {
                this.Cursor = new Cursor(Cursor.Current.Handle);
                Cursor.Position = new Point(xM, yM);
            }

        }

        private void Wylonczanie()
        {

            System.Threading.Thread.Sleep(1000);

            if (this.l4.InvokeRequired)
            {
                wypiszWylonczanie d = new wypiszWylonczanie(Wylonczanie);
                this.Invoke(d, new object[] { });
            }
            else
            {
                this.l4.Text = "Wyłączanie";
            }


            if (connectionStream != null)
            {
                connectionStream.Flush();
                connectionStream.Close(10);
               // daytatimeListener.Disconnect(true);
                //daytatimeListener.Shutdown(SocketShutdown.Both);
            }
            System.Environment.Exit(1);
        }

        void Tnij(String doCiecia)
        {

            String akcja = doCiecia.Substring(0, 1), przycisk = doCiecia.Substring(1, 1);


            if (akcja.Equals("p"))
            {

                if (przycisk.Equals("L"))
                {
                    tekstDoWypisania = "wciśnięto lewy przycisk";

                    SetCursorPos(xM, yM);
                    mouse_event(MOUSEEVENTF_LEFTDOWN, xM, yM, 0, 0);
                    mouse_event(MOUSEEVENTF_LEFTUP, xM, yM, 0, 0);

                }
                else if (przycisk.Equals("S"))
                {
                    tekstDoWypisania = "wciśnięto środkowy przycisk";

                    SetCursorPos(xM, yM);
                    mouse_event(MOUSEEVENTF_MIDDLEDOWN, xM, yM, 0, 0);
                    mouse_event(MOUSEEVENTF_MIDDLEUP, xM, yM, 0, 0);
                }
                else if (przycisk.Equals("P"))
                {
                    tekstDoWypisania = "wciśnięto prawy przycisk";

                    SetCursorPos(xM, yM);
                    mouse_event(MOUSEEVENTF_RIGHTDOWN, xM, yM, 0, 0);
                    mouse_event(MOUSEEVENTF_RIGHTUP, xM, yM, 0, 0);

                }
                else
                {
                    Wylonczanie();
                }
            }
            else
            {
                String[] xy = doCiecia.Split('x');
                xM = int.Parse(xy[0]);
                yM = int.Parse(xy[1]);
                tekstDoWypisania = "x: " + xy[0] + " y: " + xy[1];
                //ruch
                SetCursorPos(xM, yM);

            }

            SetText(tekstDoWypisania);
            void SetText(string text)
            {
                if (this.l4.InvokeRequired)
                {
                    SetTextCallback d = new SetTextCallback(SetText);
                    this.Invoke(d, new object[] { text });
                }
                else
                {
                    this.l4.Text = text;
                }
            }
        }


        public void Polaczenie()
        {
            using (daytatimeListener = new Socket(
             AddressFamily.InterNetworkV6,
             SocketType.Stream,
             ProtocolType.Tcp))
            {

                daytatimeListener.SetSocketOption(SocketOptionLevel.IPv6, (SocketOptionName)27, 0);
                IPEndPoint daytimeEndpoint = new IPEndPoint(IPAddress.IPv6Any, 49153);


                daytatimeListener.Bind(daytimeEndpoint);
                daytatimeListener.Listen(20);

                while (true)
                {
                    using (Socket incomingConnection = daytatimeListener.Accept())
                    using (connectionStream = new NetworkStream(incomingConnection, true))
                    using (StreamWriter writer = new StreamWriter(connectionStream, Encoding.ASCII))
                    using (StreamReader reader = new StreamReader(connectionStream, Encoding.UTF8))

                    {
                        writer.WriteLine(xK + "x" + yK);
                        writer.Flush();
                        ciag = reader.ReadLine();

                        SetText(ciag);

                        Tnij(ciag);

                        void SetText(string text)
                        {

                            if (this.l3.InvokeRequired)
                            {
                                SetTextCallback d = new SetTextCallback(SetText);
                                this.Invoke(d, new object[] { text });
                            }
                            else
                            {
                                this.l3.Text = text;
                            }
                        }
                    }
                }
            }
        }


        private void b1_Click(object sender, EventArgs e)
        {
            Button b1 = (Button)sender;
            Wylonczanie();

        }
    }
}
